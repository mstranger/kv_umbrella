defmodule KVServer.Application do

  # TODO: create command `help` which will return the list
  # of avaliable commands

  use Application

  def start(_type, _args) do
    # port = String.to_integer(System.get_env("PORT") || raise "missing $PORT environment variable")
    port = String.to_integer(
      System.get_env("PORT") ||
      Application.fetch_env!(:kv_server, :default_port))

    children = [
      {Task.Supervisor, name: KVServer.TaskSupervisor},
      # {Task, fn -> KVServer.accept(port) end}
      Supervisor.child_spec({Task, fn -> KVServer.accept(port) end},
        restart: :permanent)
    ]

    opts = [strategy: :one_for_one, name: KVServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
